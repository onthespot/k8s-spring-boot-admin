FROM openjdk:8-jre-alpine
COPY target/*.jar /opt/app/app.jar
WORKDIR /opt/app
ENTRYPOINT exec java $JAVA_OPTIONS -jar /opt/app/app.jar