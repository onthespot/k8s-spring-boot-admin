package company.onthespot.boot.admin.discovery;

import io.fabric8.kubernetes.client.KubernetesClient;
import org.springframework.cloud.client.discovery.AbstractDiscoveryLifecycle;
import org.springframework.stereotype.Component;

import java.util.HashMap;

@Component
public class KubernetesDiscoveryLifecycle extends AbstractDiscoveryLifecycle {

    private KubernetesClient client;

    public KubernetesDiscoveryLifecycle(KubernetesClient client) {
        this.client = client;
    }

    @Override
    protected int getConfiguredPort() {
        return client.getMasterUrl().getPort();
    }

    @Override
    protected void setConfiguredPort(int port) {

    }

    @Override
    protected Object getConfiguration() {
        return new HashMap<>();
    }

    @Override
    protected void register() {
    }

    @Override
    protected void deregister() {
    }

    @Override
    protected boolean isEnabled() {
        return true;
    }

}