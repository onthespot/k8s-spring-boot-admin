package company.onthespot.boot.admin.discovery;

import io.fabric8.kubernetes.api.model.EndpointAddress;
import io.fabric8.kubernetes.api.model.EndpointPort;
import org.springframework.cloud.client.ServiceInstance;

import java.net.URI;
import java.util.Map;

public class KubernetesServiceInstance implements ServiceInstance {

    private final String serviceId;

    private final EndpointAddress endpointAddress;

    private final EndpointPort endpointPort;

    private final Map<String, String> metadata;

    public KubernetesServiceInstance(String serviceId, EndpointAddress endpointAddress, EndpointPort endpointPort, Map<String, String> metadata) {
        this.serviceId = serviceId;
        this.endpointAddress = endpointAddress;
        this.endpointPort = endpointPort;
        this.metadata = metadata;
    }

    @Override
    public String getServiceId() {
        return serviceId;
    }

    @Override
    public String getHost() {
        return endpointAddress.getIp();
    }

    @Override
    public int getPort() {
        return endpointPort.getPort();
    }

    @Override
    public boolean isSecure() {
        return false;
    }

    @Override
    public URI getUri() {
        return URI.create(String.format("http://%s:%s", getHost(), getPort()));
    }

    @Override
    public Map<String, String> getMetadata() {
        return metadata;
    }

}
