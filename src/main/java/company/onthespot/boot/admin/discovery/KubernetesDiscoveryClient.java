package company.onthespot.boot.admin.discovery;

import io.fabric8.kubernetes.api.model.*;
import io.fabric8.kubernetes.client.KubernetesClient;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.Optional.ofNullable;

@Component
public class KubernetesDiscoveryClient implements DiscoveryClient {

    private KubernetesClient client;

    public KubernetesDiscoveryClient(KubernetesClient client) {
        this.client = client;
    }

    @Override
    public String description() {
        return "Kubernetes Discovery Client";
    }

    @Override
    public ServiceInstance getLocalServiceInstance() {
        return null;
    }

    @Override
    public List<ServiceInstance> getInstances(String serviceId) {
        Optional<Service> optService = ofNullable(client.services().withName(serviceId).get());

        if (optService.isPresent()) {

            List<EndpointSubset> subsets =
                    ofNullable(client.endpoints().withName(serviceId).get())
                            .flatMap(e -> ofNullable(e.getSubsets()))
                            .orElseGet(Collections::emptyList);

            return subsets
                    .stream()
                    .flatMap(s ->
                            ofNullable(s.getAddresses())
                                    .orElseGet(Collections::emptyList)
                                    .stream()
                                    .map(a -> new KubernetesServiceInstance(serviceId, a, findPort(s), createMetadata(optService.get()))))
                    .collect(Collectors.toList());
        } else {
            return Collections.emptyList();
        }
    }

    @Override
    public List<String> getServices() {
        ServiceList serviceList = client.services().list();
        return ofNullable(serviceList.getItems())
                .orElseGet(Collections::emptyList)
                .stream()
                .filter(this::shouldBeDiscovered)
                .map(s -> s.getMetadata().getName())
                .collect(Collectors.toList());
    }

    private Map<String, String> createMetadata(Service service) {
        return
                ofNullable(service.getMetadata().getAnnotations())
                        .map(annotations ->
                                annotations.entrySet()
                                        .stream()
                                        .filter(e -> e.getKey().startsWith("spring.io/"))
                                        .collect(Collectors.toMap(e -> e.getKey().substring(10), Map.Entry::getValue)))
                        .orElseGet(Collections::emptyMap);
    }

    private boolean shouldBeDiscovered(Service service) {
        ObjectMeta metadata = service.getMetadata();
        return metadata != null && "true".equals(ofNullable(metadata.getAnnotations()).orElseGet(HashMap::new).get("spring.io/discover"));
    }

    private EndpointPort findPort(EndpointSubset s) {
        return s.getPorts().stream().findFirst().orElseThrow(IllegalStateException::new);
    }

}
