package company.onthespot.boot.admin.discovery;

import io.fabric8.kubernetes.api.model.EndpointAddress;
import io.fabric8.kubernetes.api.model.EndpointSubset;
import io.fabric8.kubernetes.api.model.Endpoints;
import io.fabric8.kubernetes.client.KubernetesClient;
import io.fabric8.kubernetes.client.KubernetesClientException;
import io.fabric8.kubernetes.client.Watch;
import io.fabric8.kubernetes.client.Watcher;
import org.springframework.cloud.client.discovery.event.HeartbeatEvent;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.Optional.ofNullable;

@Component
public class KubernetesEndpointWatcher {

    private final ApplicationContext context;

    private final KubernetesClient client;

    private Watch watch;

    public KubernetesEndpointWatcher(ApplicationContext context, KubernetesClient client) {
        this.context = context;
        this.client = client;
    }

    @PostConstruct
    public void start() {
        Map<String, List<String>> data = new HashMap<>();
        this.watch = client.endpoints().watch(new Watcher<Endpoints>() {

            @Override
            public void eventReceived(Action action, Endpoints endpoints) {
                String service = endpoints.getMetadata().getName();

                List<EndpointSubset> subsets =
                        ofNullable(endpoints.getSubsets())
                                .orElseGet(Collections::emptyList);

                List<String> ipList = subsets
                        .stream()
                        .flatMap(s -> ofNullable(s.getAddresses()).orElseGet(Collections::emptyList).stream())
                        .map(EndpointAddress::getIp)
                        .sorted()
                        .collect(Collectors.toList());

                data.put(service, ipList);
                context.publishEvent(new HeartbeatEvent(client, data.hashCode()));
            }

            @Override
            public void onClose(KubernetesClientException cause) {
                data.clear();
            }

        });
    }

    @PreDestroy
    public void stop() {
        ofNullable(watch).ifPresent(Watch::close);
    }

}
