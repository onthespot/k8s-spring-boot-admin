package company.onthespot.boot.admin;

import de.codecentric.boot.admin.config.EnableAdminServer;
import io.fabric8.kubernetes.client.DefaultKubernetesClient;
import io.fabric8.kubernetes.client.KubernetesClient;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@EnableAdminServer
@SpringBootApplication
public class K8sSpringBootAdminApplication {

    @Bean
    public KubernetesClient kubernetesClient() {
        return new DefaultKubernetesClient();
    }

    public static void main(String[] args) {
        SpringApplication.run(K8sSpringBootAdminApplication.class, args);
    }

}
